## Sobre
Esta aplicação destina-se ao calculo de pintura de paredes de uma sala realizando a analise e calculo de informações fornecidas pelo usuário. O retorno para o usuário é a exibição das dimensões (m², nº portas/janelas) e recomendação sobre latas a serem utilizadas na pintura.

Por padrão, é seguido alguns critérios, como: Tamanho mínimo de paredes (altura e comprimento), portas, janelas e latas (Tamanhos pré-definidos).

### Requisitos
Necessário obter versão do NodeJS (v11.6.0) para instalação de dependencias do projeto.

Obs.: Esta aplicação não utiliza quaisquer conexões a banco de dados, sendo assim, rodando inteiramente no frontend para o usuário.

### Instalação
Para instalação é preciso acesso a raiz do projeto via terminal e rodar os seguintes comandos.
- Inicialmente, é necessário a instalação das dependências
```bash
npm install
```

- Para inicialização em área de desenvolvimento, utilizar:
```bash
npm start
```

### Desenvolvimento
Projeto com próposito de exibição, aprendizado e desenvolvimento de recursos e uso da linguagem React.

Feito por: Rafael Domingues Teixeira
10/07/2019