import React from 'react'
import "./App.css"

import Calculator from '../components/Calculator'

const medidas = {
    pintura:{
        _1L: 5,
        latas: [
            0.5,
            2.5,
            3.6,
            18,
        ]
    },
    
    porta: {
        width: 0.80,
        height: 1.90
    },
    janela: {
        height: 1.20,
        width: 2.00,
    },

    minimas: {
        parede:{
            width:{
                min: 1.0,
                max: 15.0,
            },
            height: {
                min: 1.0,
                max: 15.0,
            }
        }
    }
}

export default props => (
    <div className="container pt-2">        
        <div className="col-12">
            <div className="card mb-3">
                <div className="card-header bg-info text-white">
                    <h3>Calculo de pintura</h3>
                </div>
                <div className="card-body">
                    <small>
                        <div className="row">
                            <div className="col-12 col-md-6">
                                <p>Forneça os dados relativos as paredes (altura, comprimento) e serão feito os calculos da quantidade de latas necessárias para pintura. Caso necessário, inclua também o número de portas e/ou janelas.</p>
                                <p>Pode-se preencher uma ou mais paredes para análise e calculo da quantidade de latas necessárias para a superfície que será pintada.</p>
                            </div>
                            <div className="col-12 col-md-6">
                                Deve-se considerar as seguintes medidas:
                                <ul>
                                    <li>Paredes:
                                        <ul>
                                            <li>Altura:
                                                    mín. {medidas.minimas.parede.height.min.toFixed(2)}m | max. {medidas.minimas.parede.height.max.toFixed(2)}m
                                            </li>
                                            <li>Comprimento:
                                                    mín. {medidas.minimas.parede.width.min.toFixed(2)}m | max. {medidas.minimas.parede.width.max.toFixed(2)}m
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        Porta:  {medidas.porta.width.toFixed(2)} x {medidas.porta.height.toFixed(2)} (m) | 
                                        Janela:  {medidas.janela.width.toFixed(2)} x {medidas.janela.height.toFixed(2)} (m)
                                    </li>
                                    <li>Pintura:
                                        <ul>
                                            <li>{medidas.pintura._1L} m²/l de tinta </li>
                                            <li>Latas disponíveis: {medidas.pintura.latas.map((curr,i) => `${curr}L ` )}</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </small>
                </div>
            </div>
            <div className="mb-2">
                <Calculator num_paredes="4" medidas={medidas} />
            </div>
        </div>
    </div>
)