import React from 'react'

import Display from './Display'
import Parede from './Parede'

const initialState = {
    paredes: [],
    height: 0,
    width: 0,
    n_porta: 0,
    n_janela: 0,
}

export default class Calculator extends React.Component {
    constructor(props) {
        super(props)
        
        this.state = initialState;
        this.calcArea = this.calcArea.bind(this)
        this.onChange = this.onChange.bind(this);
        this.ParedeHandler = this.ParedeHandler.bind(this);
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value, result:"", error:""});

    ParedeHandler = (val) => {
        let {paredes} = this.state;
        paredes[val.num-1] = val;
        this.setState({paredes});
    }

    paredesSet(qt){
        let paredes = [];
        for (let i = 0; i < qt; i++) {
            paredes.push(<Parede key={i} num={i+1} ParedeHandler={this.ParedeHandler}/>);
        }
        return paredes;
    }

    calcArea() {
        let areas = this.state.paredes.filter((p) => [p.height, p.width, p.n_porta, p.n_janela].filter((e) => e !== "").length > 0).map((parede, i) => this.areas(parede, this.props.medidas) )
        
        let recomendacao = this.recomendacao(areas,this.props.medidas)
        this.setState({result:recomendacao})
    }    

    areas(p, med){
        let a = {
            total: p.height * p.width,
            portas: p.n_porta * (med.porta.height * med.porta.width),
            janelas: p.n_janela * (med.janela.height * med.janela.width),
            error: ""
        }

        if(a.total - (a.portas + a.janelas) < (a.total / 2))
        a.error = <React.Fragment><b>Parede {p.num}</b> - Superfície para pintura da parede é muito pequena para calculo de latas. <br /><small>(Recomendavel a remoção de uma porta e/ou janela.)</small></React.Fragment>
        if(p.n_porta > 0 && p.height < (med.porta.height + 0.3))
        a.error = <React.Fragment><b>Parede {p.num}</b> - Como há uma ou mais porta(s) atribuida(s), é necessário que altura da parede seja de no mín. {(med.porta.height + 0.3).toFixed(2)}m</React.Fragment>
        if(a.total <=0 || p.height < 1 || p.height > 15 || p.width < 1 || p.width > 15)
        a.error = <React.Fragment><b>Parede {p.num}</b> - Necessário preencher altura e comprimento. Mín: 1m e Máx: 15m</React.Fragment>

        return {
            num: p.num,
            total: a.total,
            livre: a.total - (a.portas + a.janelas),
            ocupada: (a.portas + a.janelas),
            portas: {qt: p.n_porta, m2: a.portas},
            janelas: {qt: p.n_janela, m2: a.janelas},
            error: a.error,
        }
    }

    recomendacao(areas, med){
        let area = areas.reduce((a,curr, i, array) => {
            if (a === 0) 
                a = {total: 0, livre:0, ocupada:0, portas:{qt:0, m2:0}, janelas:{qt:0, m2:0}, error: []}
            
            return {
                paredes: array,
                total: a.total+curr.total,
                livre: a.livre+curr.livre,
                ocupada: a.ocupada+curr.ocupada,
                portas:{
                    qt: Number(a.portas.qt) + Number(curr.portas.qt),
                    m2: Number(a.portas.m2) + Number(curr.portas.m2)
                },
                janelas:{
                    qt: Number(a.janelas.qt) + Number(curr.janelas.qt),
                    m2: Number(a.janelas.m2) + Number(curr.janelas.m2)
                },
                error: [...a.error, {error: curr.error},].filter((e) => e.error !== ""),                
            }
        }, 0)

        let latas = [], be_painted = area.livre;
                
        const closest = (be_painted, goal = 1.0) => med.pintura.latas.map(function(lata,i){
            return {lt:lata,qt:be_painted / (lata*med.pintura._1L), m2: lata*med.pintura._1L}
        }).reduce((prev, curr, i) => {
            return (Math.abs(curr.qt - goal) < Math.abs(prev.qt - goal) & curr.qt > 0.60 ? curr : prev);
        });

        do {
            let lata = closest(be_painted)
            let subindex = latas.findIndex(x => x.lt === lata.lt);

            if(lata.qt > 1)
               lata.qt -= lata.qt-1;

            if(latas.length > 0 && subindex !== -1){
                latas[subindex].qt += Math.round((lata.qt < 0.5 ? lata.qt + 1 : lata.qt))
                latas[subindex].m2 = latas[subindex].qt * lata.m2
            }else{
                latas.push({ 
                    lt: lata.lt, 
                    m2: lata.m2, 
                    qt: Math.round((lata.qt < 0.5 ? lata.qt + 1 : lata.qt))
                })
            }
             
            be_painted = be_painted - (lata.qt*lata.m2)
        } while (be_painted > 0);

        if(area === 0)
            area = {error: [{error:"Necessário preencher altura e comprimento de pelo menos uma parede."}]}

        return { area: area, recomendacao: latas };
    }

    render() {
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col-12 col-xl-7">
                        <div className="row">
                            {this.paredesSet(this.props.num_paredes)}
                        </div>
                    </div>
                    <div className="col-12 col-xl-5 mb-5">
                        <button className="btn btn-dark btn-block mb-3" onClick={this.calcArea}>
                            Calcular área p/ pintura
                        </button>
                        <Display result={this.state.result} error={this.state.error}/>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}