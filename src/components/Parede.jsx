import React from 'react'
import './Parede.css'

export default class Parede extends React.Component {
    
    constructor(props) {
        super(props)

        this.state = {
            num: this.props.num,
            height: "",
            width: "",
            n_porta: "",
            n_janela: "",
        }

        this.onChange = this.onChange.bind(this)
    }

    onChange(e) {
        let state = {...this.state, [e.target.name]: e.target.value}
        
        this.props.ParedeHandler(state);
        this.setState(state);
    }

    render() {
        return (
            <div className="col-12 col-md-6">
                <div className='card mb-3'>
                    <h6 className="card-header">Parede {this.props.num}</h6>
                    <div className="card-body">
                        <div className="row">
                            <div className="form-group col-12 col-sm-6">
                                <label htmlFor="height">Altura (m)</label>
                                <input type="number" name="height" id="height" className="form-control" min="0" max="15" step="0.5"
                                value={this.state.height} 
                                onChange={this.onChange}/>
                            </div>
                    
                            <div className="form-group col-12 col-sm-6">
                                <label htmlFor="width">Comprimento (m)</label>
                                <input type="number" name="width" id="width" className="form-control" min="0" max="15" step="0.5"
                                value={this.state.width} 
                                onChange={this.onChange}/>
                            </div>
                        
                            <div className="form-group col-12 col-sm-6">
                                <label htmlFor="n_porta">Nº de Portas</label>
                                <input type="number" name="n_porta" id="n_porta" className="form-control" min="0" step="1"
                                value={this.state.n_porta} 
                                onChange={this.onChange}/>
                            </div>
                            
                            <div className="form-group col-12 col-sm-6">
                                <label htmlFor="n_janela">Nº de Janelas</label>
                                <input type="number" name="n_janela" id="n_janela" className="form-control" min="0" step="1"
                                value={this.state.n_janela} 
                                onChange={this.onChange}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}