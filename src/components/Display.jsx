import React from 'react'
import './Display.css'

export default (props) => {
    let list = (result) => {
        if(result.area.error.length > 0){
            let errors = result.area.error.map((e,i) => (<li key={i}>{e.error}</li>))

            return(<div className="alert alert-danger">
                <p>Ops! Não foi possível realizar o cálculo.</p>
                <ul>{errors}</ul>
            </div>)

        }else if(result.area !== undefined){
            let paredes = result.area.paredes.map((a,i) => <li key={i}><b>Parede {a.num}</b> - Total: {a.total.toFixed(2)} m² | Livre: {a.livre.toFixed(2)}m²</li>)

            return(<div className="display-result">

            <div className="row">
                <div className="col-sm-6 col-xl-12 mb-3">
                    <div className="card h-100">
                        <h6 className="card-header bg-secondary text-white text-justify">Áreas</h6>
                        <div className="card-body">
                            <ul>
                                <li>Total (Paredes): {result.area.total.toFixed(2)}m²</li>
                                <li>Total p/ pintura: {result.area.livre.toFixed(2)}m²</li>
                                <li>Total portas/janelas: {(result.area.ocupada).toFixed(2)}m²</li>                            
                                <li> Paredes Incluídas
                                    <ul>
                                        {paredes}
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="col-sm-6 col-xl-12 mb-3">
                    <div className="card h-100">
                        <h6 className="card-header bg-success text-white text-justify">Latas a usar (Recomendável):</h6>

                        <div className="card-body bg-light">
                            <ul>
                                {result.recomendacao.map((lata,i) =>
                                    <li key={i}>{Math.round(lata.qt)} lata de <b>{lata.lt}L</b> (Rende {lata.m2.toFixed(2)}m²)</li>
                                )}
                            </ul>
                            <hr />
                            Rendem: <b>{result.recomendacao.reduce((prev,curr) => prev + curr.m2, 0).toFixed(2)}m²</b> de <b>{result.area.livre.toFixed(2)}m²</b> necessário para pintura.
                        </div>
                    </div>
                </div>
            </div>

            
            <div className="alert alert-warning">
                <h6 className="alert-heading">Atenção!</h6>
                A recomendação é feita com base em latas já pré-cadastradas. Não são levados em consideração nenhum valor, apenas a quantidade em litros das latas.
            </div>
            </div>)
        }
    }

    return (<div>{ props.result !== undefined ? list(props.result): null } </div>)
}